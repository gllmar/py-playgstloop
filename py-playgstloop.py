import gi
import sys
import argparse
import subprocess
import os

gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def on_message(bus, message, loop, player):
    if message.type == Gst.MessageType.EOS:
        player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, 0)
    elif message.type == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(f"Error: {err}, {debug}", file=sys.stderr)
        loop.quit()
    return True

def clear_framebuffer():
    try:
        subprocess.run(['dd', 'if=/dev/zero', 'of=/dev/fb0'], check=True, text=True, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        print(f"Failed to clear framebuffer: {e.stderr}", file=sys.stderr)


def disable_cursor_blinking():
    os.system("echo -e '\033[?17;0;0c' > /dev/tty1")
    # Ensure cursor gets turned back on when script exits
    os.system('trap "echo -e \'\033[?17;14;224c\' > /dev/tty1" EXIT')

def get_codec_ffprobe(filepath):
    command = [
        'ffprobe',
        '-v', 'error',
        '-select_streams', 'v:0',
        '-show_entries', 'stream=codec_name',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        filepath
    ]
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if result.returncode == 0:
        return result.stdout.strip()
    else:
        print(f"Failed to get codec: {result.stderr}", file=sys.stderr)
        sys.exit(1)

def get_available_decoders(codec):
    registry = Gst.Registry.get()
    decoders = []
    for feature in registry.get_feature_list(Gst.ElementFactory):
        if 'Decoder/Video' in feature.get_klass() and codec.lower() in feature.get_name().lower():
            decoders.append(feature.get_name())
    return decoders

def select_decoder(decoders):
    if not decoders:
        print("No available decoders found.")
        sys.exit(1)

    print("Available decoders:")
    for i, decoder in enumerate(decoders, 1):
        print(f"{i}. {decoder}")

    try:
        selected = int(input("Select a decoder by number: ")) - 1
        if not (0 <= selected < len(decoders)):
            raise ValueError
    except ValueError:
        print("Invalid selection.")
        sys.exit(1)

    return decoders[selected]


def get_available_sinks():
    registry = Gst.Registry.get()
    sinks = []
    for feature in registry.get_feature_list(Gst.ElementFactory):
        if 'Sink/Video' in feature.get_klass():
            sinks.append(feature.get_name())
    return sinks

def select_sink():
    sinks = get_available_sinks()
    if not sinks:
        print("No available video sinks found.")
        sys.exit(1)

    print("Available video sinks:")
    for i, sink in enumerate(sinks, 1):
        print(f"{i}. {sink}")

    try:
        selected = int(input("Select a sink by number (or press Enter to use autovideosink): ")) - 1
        if not (0 <= selected < len(sinks)):
            raise ValueError
    except ValueError:
        print("Defaulting to autovideosink.")
        return 'autovideosink'

    return sinks[selected]

def main():
    Gst.init(None)

    parser = argparse.ArgumentParser(description='Play a video file in a seamless loop.')
    parser.add_argument('filepath', help='The path to the video file to play.')
    parser.add_argument('--decoder', help='The decoder to use.')
    parser.add_argument('--sink', help='The video sink to use.')
    parser.add_argument('--loopback', action='store_true', help='Enable loopback device output.')
    parser.add_argument('--loopbackdev', default='/dev/video61', help='Loopback device address. Default is /dev/video61.')
    args = parser.parse_args()

    loop = GLib.MainLoop()

    codec = get_codec_ffprobe(args.filepath)
    print(f"Codec: {codec}")

    if args.decoder:
        decoder_name = args.decoder
    else:
        decoders = get_available_decoders(codec)
        decoder_name = select_decoder(decoders)
    
    if args.sink:
        sink_name = args.sink
    else:
        sink_name = select_sink()

    if sink_name == 'fbdevsink':
        clear_framebuffer()
        disable_cursor_blinking()
        clear_framebuffer() 

    # If loopback is enabled, override the sink_name
    if args.loopback:
        sink_name = f'v4l2sink device={args.loopbackdev}'

    if codec.lower() == 'h264':
        pipeline_desc = f'filesrc location={args.filepath} ! qtdemux ! h264parse ! {decoder_name} ! videoconvert ! {sink_name}'
    else:
        pipeline_desc = f'filesrc location={args.filepath} ! qtdemux ! {decoder_name} ! videoconvert ! {sink_name}'

    gst_launch_command = f"gst-launch-1.0 {pipeline_desc}"
    print(f"Executable GST Pipeline: {gst_launch_command}")  # Print the executable gst-launch-1.0 command

    player = Gst.parse_launch(pipeline_desc)
    bus = player.get_bus()
    bus.add_watch(0, on_message, loop, player)

    player.set_state(Gst.State.PLAYING)

    try:
        loop.run()
    except KeyboardInterrupt:
        pass

    player.set_state(Gst.State.NULL)

if __name__ == '__main__':
    main()